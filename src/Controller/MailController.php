<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class MailController extends AbstractController
{
    public function sendMultiple(Request $request)
    {
        $transport = (new \Swift_SmtpTransport('smtp.gmail.com', 587, 'tls'));
        $transport->setUsername('jordansprivatemailer@gmail.com');
        $transport->setPassword('reliamsnadroj');

        $mailer = new \Swift_Mailer($transport);

        $mails = json_decode($request->getContent(), true);
        foreach ($mails as $mail => $jsons) {
            foreach($jsons as $index => $content) {
                $message = new \Swift_Message();
                $message->setSubject($content['subject']);
                $message->setFrom(['jordansprivatemailer@gmail.com' => 'Projet voyage Digitalcube']);
                $message->setTo($content['mail_to']);
                $message->setBody($content['msg_html'],'text/html');
                $message->addPart($content['msg_txt'], 'text/plain');

                /*if($request->get($content['attachment'] != '')) {
                    $data = base64_decode($content['attachment']);
                    file_put_contents($_SERVER['DOCUMENT_ROOT']."\\travelogue_pdf\\tmp.pdf", $data);
    
                    $message -> attach(\Swift_Attachment::fromPath($_SERVER['DOCUMENT_ROOT']."\\travelogue_pdf\\tmp.pdf"));
    
                    unlink($_SERVER['DOCUMENT_ROOT']."\\travelogue_pdf\\tmp.pdf");
                }*/

                try {
                    $result = $mailer->send($message);
                }
                catch (\Swift_TransportException $e) {
                    $result = ['failSending' => $e->getMessage()];
                }
           }
        }

        return new Response(json_encode(['message' => 'Mails successfully sent.']),
        Response::HTTP_CREATED,
        ['content-type' => 'application/json']);
    }
}