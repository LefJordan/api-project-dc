<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class AuthController extends AbstractController
{
    public function register(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $em = $this->getDoctrine()->getManager();
        
        $email = $request->request->get('email');
        $password = $request->request->get('password');
        $firstname = $request->request->get('firstname');
        $lastname = $request->request->get('lastname');
        $roles = $request->request->get('roles');
        
        $user = new User();
        $user->setEmail($email);
        $user->setPassword($encoder->encodePassword($user, $password));
        $user->setFirstname($firstname);
        $user->setLastname($lastname);

        if($roles) {
            $user->setRoles($roles);
        }

        $em->persist($user);
        $em->flush();

        return new Response(json_encode(['message' => 'User '.$user->getUsername().' successfully created.']),
        Response::HTTP_CREATED,
        ['content-type' => 'application/json']);
    }

    public function loginTwig(AuthenticationUtils $authenticationUtils)
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last email entered by the user
        $lastEmail = $authenticationUtils->getLastUsername();

        return $this->render('login.html.twig', [
            'last_email' => $lastEmail,
            'error'         => $error,
        ]);
    }
}