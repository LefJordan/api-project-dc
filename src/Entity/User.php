<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(name="user")
 * @ORM\Entity
 * @ApiResource(
 *     normalizationContext={"groups"={"user", "user:read"}},
 *     denormalizationContext={"groups"={"user", "user:write"}},
 *     attributes={"security"="is_granted('ROLE_USER')"},
 *     collectionOperations={
 *         "get",
 *         "post"
 *     },
 *     itemOperations={
 *         "delete",
 *         "get",
 *         "put"={"security"="is_granted('ROLE_ADMIN') or object == user"},
 *     }
 * )
 * @ApiFilter(SearchFilter::class, properties={"email": "exact"})
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"user"})
     */
    public $id;

    /**
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     * @Groups({"user", "travelogue:read"})
     * @Assert\NotBlank()
     * @Assert\Email
     */
    public $email;

    /**
     * @ORM\Column(type="string", length=500)
     * @Groups({"user:write"})
     * @Assert\NotBlank()
     */
    public $password;

    /**
     * @ORM\Column(name="firstname", type="string", length=20)
     * @Groups({"user", "team:read"})
     * @Assert\NotBlank()
     */
    public $firstname;

    /**
     * @ORM\Column(name="lastname", type="string", length=30)
     * @Groups({"user", "team:read"})
     * @Assert\NotBlank()
     */
    public $lastname;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    public $isActive;

    /**
    * @ORM\Column(type="array")
    * @Groups({"user"})
    */
    public $roles = [];

    public function __construct()
    {
        $this->isActive = true;
        $this->roles = ['ROLE_ADMIN'];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    // mandatory method for UserInterface needed by the security system 
    public function getUsername()
    {
        return $this->email;
    }

    public function getSalt()
    {
        return null;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function addRole($role) {
        $this->roles[] = $role;
    }

    public function setRoles($roles) {
        $this->roles = $roles;
    }

    public function eraseCredentials()
    {
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getFirstname()
    {
        return $this->firstname;
    }

    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getLastname()
    {
        return $this->lastname;
    }
}