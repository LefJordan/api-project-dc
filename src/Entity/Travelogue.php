<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Table(name="travelogue")
 * @ORM\Entity
 * @ApiResource(
 *      normalizationContext={"groups"={"travelogue:read"}},
 *      attributes={"security"="is_granted('ROLE_USER')"},
 *      collectionOperations={
 *          "get",
 *          "post"={"security"="is_granted('ROLE_ADMIN')"}
 *      },
 *      itemOperations={
 *          "delete",
 *          "get",
 *          "put"={"security"="is_granted('ROLE_ADMIN')"},
 *      }
 * )
 * @ApiFilter(SearchFilter::class, properties={"teams.users.id": "exact"})
 */
class Travelogue
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"travelogue:read"})
     */
    public $id;

    /**
     * @ORM\Column(type="string")
     * @Groups({"travelogue:read", "team:read"})
     * @Assert\NotBlank()
     */
    public $name;

    /**
     * @ORM\Column(type="text")
     * @Groups({"travelogue:read"})
     * @Assert\NotBlank()
     */
    public $description;

    /**
     * @ORM\Column(name="pdf_name", type="string")
     * @Groups({"travelogue:read"})
     */
    public $pdfName;

    /**
     * @var MediaObject|null
     * 
     * @ORM\ManyToOne(targetEntity=MediaObject::class, cascade={"remove"})
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"travelogue:read"})
     */
    public $pdfLink;

    /**
     * @ORM\OneToMany(targetEntity="Team", mappedBy="travelogue", cascade={"persist", "remove"})
     * @Groups({"travelogue:read"})
     */
    public $teams;
    
    public function __construct()
    {
        $this->teams = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getPdfName()
    {
        return $this->pdfName;
    }

    public function setPdfName($pdfName)
    {
        $this->pdfName = $pdfName;
    }

    public function getPdfLink()
    {
        return $this->pdfLink;
    }

    public function setPdfLink($pdfLink)
    {
        $this->pdfLink = $pdfLink;
    }

    public function addTeam(Team $team)
    {
        $team->travelogue = $this;
        $this->teams->add($team);
    }

    public function removeTeam(Team $team)
    {
        $team->travelogue = null;
        $this->teams->removeElement($team);
    }
}