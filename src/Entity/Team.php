<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="team")
 * @ORM\Entity
 * @ApiResource(
 *      normalizationContext={"groups"={"team:read"}},
 *      attributes={"security"="is_granted('ROLE_USER')"},
 *      collectionOperations={
 *          "get",
 *          "post"={"security"="is_granted('ROLE_ADMIN')"}
 *      },
 *      itemOperations={
 *          "delete",
 *          "get",
 *          "put"={"security"="is_granted('ROLE_ADMIN')"},
 *      }
 * )
 * @ApiFilter(SearchFilter::class, properties={"users.id": "exact"})
 */
class Team
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"team:read"})
     */
    public $id;

    /**
     * @ORM\Column(type="string", unique=true)
     * @Groups({"team:read", "travelogue:read"})
     * @Assert\NotBlank()
     */
    public $name;

    /**
    * @ORM\ManyToMany(targetEntity="App\Entity\User", cascade={"persist"})
    * @Groups({"team:read", "travelogue:read"})
    */
    public $users;

    /**
    * @ORM\ManyToOne(targetEntity="App\Entity\Travelogue", inversedBy="teams")
    * @Groups({"team:read"})
    * @Assert\NotBlank()
    */
    public $travelogue;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function addUser(User $user)
    {
        $this->users[] = $user;
    }

    public function removeUser(User $user)
    {
        $this->users->removeElement($user);
    }

    public function getUsers()
    {
        return $this->users;
    }

    public function setTravelogue(Travelogue $travelogue)
    {
        $this->travelogue = $travelogue;

        return $this;
    }

    public function getTravelogue()
    {
        return $this->travelogue;
    }
}